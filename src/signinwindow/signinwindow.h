#ifndef SIGN_IN_WINDOW_H
#define SIGN_IN_WINDOW_H

#include <QDialog>
#include "settingsparser/settingsparser.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class signInWindow;
}
QT_END_NAMESPACE

class SignInWindow : public QDialog
{
    Q_OBJECT
public:
    SignInWindow(QWidget *parent = nullptr);
    ~SignInWindow();

    void setSettingsPath(const QString &path);
public slots:
    void requireAuthorization();

signals:
    void userAuthorized(const Settings &, const QString &login, const QString &password);

private:
    Ui::signInWindow *m_ui;
    Settings m_settings;
    SettingsParser m_parser;
    QString m_path;

    void m_getSettings();
    User m_findUser(const QString &login) const;
    bool m_isCorrectPassword(const User &user, const QString &password) const;

    void m_addUser(const User &newUser);

private slots:
    void m_signIn();
    void m_registerUser();
};

#endif