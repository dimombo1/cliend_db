#include "signinwindow.h"

#include <algorithm>
#include <cstdlib>

#include <QCryptographicHash>
#include <QDebug>
#include <QJsonDocument>
#include <QProcess>
#include <QStringList>
#include <QFile>

#include "ui_signinwindow.h"

SignInWindow::SignInWindow(QWidget *parent) : QDialog(parent), m_ui(new Ui::signInWindow)
{
    m_ui->setupUi(this);
    m_ui->wrongSignInLabel->setVisible(false);
    QObject::connect(m_ui->signInPushButton, &QPushButton::clicked, this, &SignInWindow::m_signIn);
    QObject::connect(m_ui->registerButton, &QPushButton::pressed, this, &SignInWindow::m_registerUser);
}

SignInWindow::~SignInWindow()
{
    delete m_ui;
}

void SignInWindow::setSettingsPath(const QString &path)
{
    m_path = path;
    m_parser.getSettingsFromFile(path);
    m_settings = m_parser.parse();
}

void SignInWindow::requireAuthorization()
{
    setVisible(true);
}

void SignInWindow::m_signIn()
{
    QString login = m_ui->loginEdit->text();
    QString password = m_ui->passwordEdit->text();
    User user = m_findUser(login);

    if (user.login.isEmpty() && !m_ui->wrongSignInLabel->isVisible()) {
        qDebug() << "No such user found " << login;
        m_ui->wrongSignInLabel->setVisible(true);
        return;  
    } else if (user.login.isEmpty()) {
        qDebug() << "No such user found " << login;
        return;
    }

    if (!m_isCorrectPassword(user, password) && !m_ui->wrongSignInLabel->isVisible()) {
        qDebug() << "Wrong password for " << login;
        m_ui->wrongSignInLabel->setVisible(true);
        return;
    } else if (!m_isCorrectPassword(user, password)) {
        qDebug() << "Wrong password for " << login;
        return;
    }

    setVisible(false);
    emit userAuthorized(m_settings, login, password);
}

void SignInWindow::m_registerUser()
{
    QString login = m_ui->loginEdit->text();
    QString password = m_ui->passwordEdit->text();
    User user = m_findUser(login);

    if (!user.login.isEmpty()) {
        qDebug() << "Already has user with login " << login;
        return;
    }

    QString hashedPassword =
            QCryptographicHash::hash(password.toUtf8(), QCryptographicHash::Sha256).toHex();

    User newUser { login, hashedPassword };
    m_settings.users.insert(login, newUser);
    QJsonArray newSettings = SettingsParser::toJsonSettings(m_settings);
    
    QFile file(m_path);

    if (!file.open(QIODevice::WriteOnly)) {
        qDebug() << "Unable to open " << m_path;
        return;
    }

    QJsonDocument jsonDocument;
    jsonDocument.setArray(newSettings);

    QByteArray jsonData = jsonDocument.toJson();
    
    auto written = file.write(jsonData);
    if (written == -1) {
        qDebug() << "Error writing to file " << m_path;
        return;
    }

    file.close();

    QStringList args;
    args << "-U" << "user";
    args << "-d" << m_settings.databaseName;
    args << "-c" << QString("CREATE USER %1 WITH PASSWORD \'%2\'").arg(login).arg(password);

    QProcess psql;
    psql.start("psql", args);
    psql.waitForFinished(-1);

    if (psql.exitCode()) {
        qDebug() << "error creating user" << login << psql.errorString();
        return;
    } else {
        qDebug() << "new user " << login << " registered";
    }

    const QString command = QString("psql -U user -d lab2 -c \'GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO %1\'").arg(login);

    int returnable = std::system(command.toStdString().c_str());
    if (returnable) {
        qDebug() << "error granting privileges to " << login << "exit code = " << returnable;
    } else {
        qDebug() << "privileges granted to " << login;
    }
}

bool SignInWindow::m_isCorrectPassword(const User &user, const QString &password) const
{
    QString hashedPassword =
            QCryptographicHash::hash(password.toUtf8(), QCryptographicHash::Sha256).toHex();

    return (hashedPassword == user.password);
}

User SignInWindow::m_findUser(const QString &login) const
{
    const auto & users = m_settings.users;
    auto user = users.find(login);
    if (user == users.end()) {
        return {QString(), QString()};
    }
    return *user;
}
