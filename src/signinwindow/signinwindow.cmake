if (NOT SIGN_IN_WINDOW_INCLUDED)
    set(SIGN_IN_WINDOW_INCLUDED TRUE)
    include_directories(${CMAKE_CURRENT_LIST_DIR})

    set(SIGN_IN_WINDOW
        ${CMAKE_CURRENT_LIST_DIR}/signinwindow.h
        ${CMAKE_CURRENT_LIST_DIR}/signinwindow.cpp
        ${CMAKE_CURRENT_LIST_DIR}/signinwindow.ui
    )
endif()