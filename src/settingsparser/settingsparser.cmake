if (NOT SETTINGS_PARSER_INCLUDED)
    set(SETTINGS_PARSER_INCLUDED TRUE)
    include_directories(${CMAKE_CURRENT_LIST_DIR})

    set(SETTINGS_PARSER
        ${CMAKE_CURRENT_LIST_DIR}/settingsparser.h
        ${CMAKE_CURRENT_LIST_DIR}/settingsparser.cpp
        )
endif()