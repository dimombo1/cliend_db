#ifndef SETTINGS_PARSER_H
#define SETTINGS_PARSER_H

#include <QString>
#include <QJsonArray>
#include "settings/settings.h"

class SettingsParser
{
public:
    SettingsParser() = default;
    void getSettingsFromFile(const QString &path);
    Settings parse() const;
    static QJsonArray toJsonSettings(const Settings &settings);

private:
    QJsonArray m_settings;
};

#endif