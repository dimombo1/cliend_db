#include "settingsparser.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

#include <QDebug>

void SettingsParser::getSettingsFromFile(const QString &path)
{
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Failed to open file";
        return;
    }

    QJsonParseError parseError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(file.readAll(), &parseError);

    file.close();

    if (parseError.error != QJsonParseError::NoError) {
        qDebug() << "JSON parse error: " << parseError.errorString();
        return;
    }

    if (!jsonDoc.isArray()) {
        qDebug() << "JSON document is not an array";
        return;
    }

    m_settings = jsonDoc.array();

    if (m_settings.size() != Settings::getNumberOfSettings()) {
        m_settings = QJsonArray();
        qDebug() << "Wrong number of settings (must be 5)";
        return;
    }
}

Settings SettingsParser::parse() const
{
    auto users = m_settings[3].toArray();
    QMap< QString, User > usersMap;
    for (auto && user: users)
    {
        QString login = user.toObject().value("login").toString();
        QString password = user.toObject().value("password").toString();
        usersMap.insert(login, std::move(User{ login, password }));
    }

    return Settings{ m_settings[0].toString(), m_settings[1].toString(), m_settings[2].toString(),
                     usersMap };
}

QJsonArray SettingsParser::toJsonSettings(const Settings &settings)
{
    QJsonArray returnable;
    returnable.append(settings.type);
    returnable.append(settings.connectionName);
    returnable.append(settings.databaseName);
    QJsonArray users;

    for (auto && user: settings.users) {
        users.append(QJsonObject({QPair< QString, QString >(QStringLiteral("login"), user.login),
                                    QPair< QString, QString >(QStringLiteral("password"), user.password)}));
    }

    returnable.append(users);
    return returnable;
}