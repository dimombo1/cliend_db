#include <QApplication>
#include "signinwindow/signinwindow.h"
#include "mainwindow/mainwindow.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    const QString path = "db_settings.json";

    SignInWindow signIn;
    signIn.setSettingsPath(path);
    signIn.show();

    MainWindow mainWindow;

    QObject::connect(&signIn, &SignInWindow::userAuthorized, &mainWindow,
                     &MainWindow::userIsAuthorized);
    QObject::connect(&mainWindow, &MainWindow::sessionEnded, &signIn, &SignInWindow::requireAuthorization);

    return app.exec();
}