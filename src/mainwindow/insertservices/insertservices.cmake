if (NOT INSERT_SERVICES_INCLUDED)
    set(INSERT_SERVICES_INCLUDED TRUE)
    include_directories(${CMAKE_CURRENT_LIST_DIR})

    set(INSERT_SERVICES
        ${CMAKE_CURRENT_LIST_DIR}/insertservices.cpp
        ${CMAKE_CURRENT_LIST_DIR}/insertservices.ui
        )
endif()