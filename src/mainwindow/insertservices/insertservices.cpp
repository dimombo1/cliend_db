#include "mainwindow.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlTableModel>
#include <QDebug>

#include "ui_insertservices.h"

MainWindow::InsertServicesWindow::InsertServicesWindow(QWidget *parent)
    : QDialog(parent), m_ui(new Ui::InsertServicesWindow)
{
    m_ui->setupUi(this);
    QObject::connect(m_ui->buttonBox, &QDialogButtonBox::accepted, this,
                     &InsertServicesWindow::m_insertIntoServices);
}

MainWindow::InsertServicesWindow::~InsertServicesWindow()
{
    delete m_ui;
}

void MainWindow::InsertServicesWindow::m_insertIntoServices()
{
    int id = m_ui->id->value();
    QString name = m_ui->name->text();
    double costForeign = m_ui->costForeign->value();
    double costOur = m_ui->costOur->value();

    QString queryText;

    if (id == -1) {
        queryText = QString("INSERT INTO services (name, cost_foreign, cost_our) values (\'%1\', %2, %3);")
                            .arg(name)
                            .arg(costForeign)
                            .arg(costOur);
    } else {
        queryText =
                QString("INSERT INTO services (id, name, cost_foreign, cost_our) values (%1, \'%2\', %3, %4);")
                        .arg(id)
                        .arg(name)
                        .arg(costForeign)
                        .arg(costOur);
    }
    QSqlQuery query;
    if (!query.exec(queryText)) {
        qDebug() << "Unable to execute query";
        qDebug() << query.lastError().text();
    } else {
        m_model->select();
    }
    
    emit event(queryText);

    accept();
}

void MainWindow::InsertServicesWindow::setModel(std::shared_ptr< QSqlTableModel > model)
{
    m_model = model;
}