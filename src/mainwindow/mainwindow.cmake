if (NOT MAIN_WINDOW_INCLUDED)
    set(MAIN_WINDOW_INCLUDED TRUE)
    include_directories(${CMAKE_CURRENT_LIST_DIR})

    include (${CMAKE_CURRENT_LIST_DIR}/insertservices/insertservices.cmake)
    include (${CMAKE_CURRENT_LIST_DIR}/insertmasters/insertmasters.cmake)
    include (${CMAKE_CURRENT_LIST_DIR}/insertcars/insertcars.cmake)

    include (${CMAKE_CURRENT_LIST_DIR}/updateservices/updateservices.cmake)
    include (${CMAKE_CURRENT_LIST_DIR}/updatemasters/updatemasters.cmake)
    include (${CMAKE_CURRENT_LIST_DIR}/updatecars/updatecars.cmake)

    include (${CMAKE_CURRENT_LIST_DIR}/deletefromtable/deletefromtable.cmake)

    set(MAIN_WINDOW
        ${CMAKE_CURRENT_LIST_DIR}/mainwindow.h
        ${CMAKE_CURRENT_LIST_DIR}/mainwindow.cpp
        ${CMAKE_CURRENT_LIST_DIR}/mainwindow.ui
        ${INSERT_SERVICES}
        ${INSERT_MASTERS}
        ${INSERT_CARS}
        ${UPDATE_SERVICES}
        ${UPDATE_MASTERS}
        ${UPDATE_CARS}
        ${DELETE_FROM_TABLE}
        )
endif()