#include "mainwindow.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlTableModel>
#include <QDebug>

#include "ui_updatecars.h"

MainWindow::UpdateCarsWindow::UpdateCarsWindow(QWidget *parent)
    : QDialog(parent), m_ui(new Ui::UpdateCarsWindow)
{
    m_ui->setupUi(this);
    QObject::connect(m_ui->buttonBox, &QDialogButtonBox::accepted, this,
                     &UpdateCarsWindow::m_updateCars);
}

MainWindow::UpdateCarsWindow::~UpdateCarsWindow()
{
    delete m_ui;
}

void MainWindow::UpdateCarsWindow::m_updateCars()
{
    QString newNum = m_ui->newNum->toPlainText();
    newNum = (newNum == "num") ? newNum : QString("\'%1\'").arg(newNum);

    QString newColor = m_ui->newColor->toPlainText();
    newColor = (newColor == "color") ? newColor : QString("\'%1\'").arg(newColor);

    QString newMark = m_ui->newMark->toPlainText();
    newMark = (newMark == "mark") ? newMark : QString("\'%1\'").arg(newMark);

    QString newIsForeign = m_ui->newIsForeign->toPlainText();
    newIsForeign = (newIsForeign == "is_foreign") ? newIsForeign : QString("\'%1\'").arg(newIsForeign);

    QString where = m_ui->where->toPlainText();

    QString queryText;

    queryText = QString("UPDATE cars SET num = %1, color = %2, mark = %3, is_foreign = %4 WHERE %5;")
                        .arg(newNum)
                        .arg(newColor)
                        .arg(newMark)
                        .arg(newIsForeign)
                        .arg(where);
    QSqlQuery query;
    if (!query.exec(queryText)) {
        qDebug() << "Unable to execute query";
        qDebug() << query.lastError().text();
    } else {
        m_model->select();
    }

    emit event(queryText);

    accept();
}

void MainWindow::UpdateCarsWindow::setModel(std::shared_ptr< QSqlTableModel > model)
{
    m_model = model;
}