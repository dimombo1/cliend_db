if (NOT UPDATE_CARS_INCLUDED)
    set(UPDATE_CARS_INCLUDED TRUE)
    include_directories(${CMAKE_CURRENT_LIST_DIR})

    set(UPDATE_CARS
        ${CMAKE_CURRENT_LIST_DIR}/updatecars.cpp
        ${CMAKE_CURRENT_LIST_DIR}/updatecars.ui
        )
endif()