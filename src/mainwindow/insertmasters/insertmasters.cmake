if (NOT INSERT_MASTERS_INCLUDED)
    set(INSERT_MASTERS_INCLUDED TRUE)
    include_directories(${CMAKE_CURRENT_LIST_DIR})

    set(INSERT_MASTERS
        ${CMAKE_CURRENT_LIST_DIR}/insertmasters.cpp
        ${CMAKE_CURRENT_LIST_DIR}/insertmasters.ui
        )
endif()