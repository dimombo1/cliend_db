#include "mainwindow.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlTableModel>
#include <QDebug>

#include "ui_insertmasters.h"

MainWindow::InsertMastersWindow::InsertMastersWindow(QWidget *parent)
    : QDialog(parent), m_ui(new Ui::InsertMastersWindow)
{
    m_ui->setupUi(this);
    QObject::connect(m_ui->buttonBox, &QDialogButtonBox::accepted, this,
                     &InsertMastersWindow::m_insertIntoMasters);
}

MainWindow::InsertMastersWindow::~InsertMastersWindow()
{
    delete m_ui;
}

void MainWindow::InsertMastersWindow::m_insertIntoMasters()
{
    int id = m_ui->id->value();
    QString name = m_ui->name->text();
    QString queryText;

    if (id == -1) {
        queryText = QString("INSERT INTO masters (name) values (\'%1\');").arg(name);
    } else {
        queryText =
                QString("INSERT INTO masters (id, name) values (%1, \'%2\');")
                        .arg(id)
                        .arg(name);
    }
    QSqlQuery query;
    if (!query.exec(queryText)) {
        qDebug() << "Unable to execute query";
        qDebug() << query.lastError().text();
    } else {
        m_model->select();
    }
    
    emit event(queryText);
    
    accept();
}

void MainWindow::InsertMastersWindow::setModel(std::shared_ptr< QSqlTableModel > model)
{
    m_model = model;
}