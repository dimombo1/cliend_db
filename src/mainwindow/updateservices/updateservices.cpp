#include "mainwindow.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlTableModel>
#include <QDebug>

#include "ui_updateservices.h"

MainWindow::UpdateServicesWindow::UpdateServicesWindow(QWidget *parent)
    : QDialog(parent), m_ui(new Ui::UpdateServicesWindow)
{
    m_ui->setupUi(this);
    QObject::connect(m_ui->buttonBox, &QDialogButtonBox::accepted, this,
                     &UpdateServicesWindow::m_updateServices);
}

MainWindow::UpdateServicesWindow::~UpdateServicesWindow()
{
    delete m_ui;
}

void MainWindow::UpdateServicesWindow::m_updateServices()
{
    QString newName = m_ui->newName->toPlainText();
    newName = (newName == "name") ? newName : QString("\'%1\'").arg(newName);

    QString newCostForeign = m_ui->newCostForeign->toPlainText();
    QString newCostOur = m_ui->newCostOur->toPlainText();
    QString where = m_ui->where->toPlainText();

    QString queryText;

    queryText = QString("UPDATE services SET name = %1, cost_foreign = %2, cost_our = %3 WHERE %4;")
                        .arg(newName)
                        .arg(newCostForeign)
                        .arg(newCostOur)
                        .arg(where);
    QSqlQuery query;
    if (!query.exec(queryText)) {
        qDebug() << "Unable to execute query";
        qDebug() << query.lastError().text();
    } else {
        m_model->select();
    }

    emit event(queryText);

    accept();
}

void MainWindow::UpdateServicesWindow::setModel(std::shared_ptr< QSqlTableModel > model)
{
    m_model = model;
}