if (NOT UPDATE_SERVICES_INCLUDED)
    set(UPDATE_SERVICES_INCLUDED TRUE)
    include_directories(${CMAKE_CURRENT_LIST_DIR})

    set(UPDATE_SERVICES
        ${CMAKE_CURRENT_LIST_DIR}/updateservices.cpp
        ${CMAKE_CURRENT_LIST_DIR}/updateservices.ui
        )
endif()