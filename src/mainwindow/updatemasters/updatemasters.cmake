if (NOT UPDATE_MASTERS_INCLUDED)
    set(UPDATE_MASTERS_INCLUDED TRUE)
    include_directories(${CMAKE_CURRENT_LIST_DIR})

    set(UPDATE_MASTERS
        ${CMAKE_CURRENT_LIST_DIR}/updatemasters.cpp
        ${CMAKE_CURRENT_LIST_DIR}/updatemasters.ui
        )
endif()