#include "mainwindow.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlTableModel>
#include <QDebug>

#include "ui_updatemasters.h"

MainWindow::UpdateMastersWindow::UpdateMastersWindow(QWidget *parent)
    : QDialog(parent), m_ui(new Ui::UpdateMastersWindow)
{
    m_ui->setupUi(this);
    QObject::connect(m_ui->buttonBox, &QDialogButtonBox::accepted, this,
                     &UpdateMastersWindow::m_updateMasters);
}

MainWindow::UpdateMastersWindow::~UpdateMastersWindow()
{
    delete m_ui;
}

void MainWindow::UpdateMastersWindow::m_updateMasters()
{
    QString newName = m_ui->newName->toPlainText();
    newName = (newName == "name") ? newName : QString("\'%1\'").arg(newName);
    QString where = m_ui->where->toPlainText();

    QString queryText;

    queryText = QString("UPDATE masters SET name = %1 WHERE %2;")
                        .arg(newName)
                        .arg(where);
    QSqlQuery query;
    if (!query.exec(queryText)) {
        qDebug() << "Unable to execute query";
        qDebug() << query.lastError().text();
    } else {
        m_model->select();
    }

    emit event(queryText);

    accept();
}

void MainWindow::UpdateMastersWindow::setModel(std::shared_ptr< QSqlTableModel > model)
{
    m_model = model;
}