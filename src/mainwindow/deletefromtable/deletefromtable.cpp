#include "mainwindow.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlTableModel>
#include <QDebug>

#include "ui_deletefromtable.h"

MainWindow::DeleteFromTableWindow::DeleteFromTableWindow(QWidget *parent)
    : QDialog(parent), m_ui(new Ui::DeleteFromTableWindow)
{
    m_ui->setupUi(this);
    QObject::connect(m_ui->buttonBox, &QDialogButtonBox::accepted, this,
                     &DeleteFromTableWindow::m_deleteFromTable);
}

MainWindow::DeleteFromTableWindow::~DeleteFromTableWindow()
{
    delete m_ui;
}

void MainWindow::DeleteFromTableWindow::m_deleteFromTable()
{
    QString where = m_ui->where->toPlainText();
    QString queryText;

    queryText = QString("DELETE FROM %1 WHERE %2;")
                .arg(m_tableName)
                .arg(where);

    QSqlQuery query;
    if (!query.exec(queryText)) {
        qDebug() << "Unable to execute query";
        qDebug() << query.lastError().text();
    } else {
        m_model->select();
    }

    emit event(queryText);

    accept();
}

void MainWindow::DeleteFromTableWindow::setModel(std::shared_ptr< QSqlTableModel > model)
{
    m_model = model;
}

void MainWindow::DeleteFromTableWindow::setTable(const QString &tableName)
{
    m_tableName = tableName;
}