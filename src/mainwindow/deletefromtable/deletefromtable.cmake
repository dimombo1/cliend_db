if (NOT DELETE_FROM_TABLE_INCLUDED)
    set(DELETE_FROM_TABLE_INCLUDED TRUE)
    include_directories(${CMAKE_CURRENT_LIST_DIR})

    set(DELETE_FROM_TABLE
        ${CMAKE_CURRENT_LIST_DIR}/deletefromtable.cpp
        ${CMAKE_CURRENT_LIST_DIR}/deletefromtable.ui
        )
endif()