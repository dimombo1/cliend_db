if (NOT INSERT_CARS_INCLUDED)
    set(INSERT_CARS_INCLUDED TRUE)
    include_directories(${CMAKE_CURRENT_LIST_DIR})

    set(INSERT_CARS
        ${CMAKE_CURRENT_LIST_DIR}/insertcars.cpp
        ${CMAKE_CURRENT_LIST_DIR}/insertcars.ui
        )
endif()