#include "mainwindow.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlTableModel>
#include <QDebug>

#include "ui_insertcars.h"

MainWindow::InsertCarsWindow::InsertCarsWindow(QWidget *parent)
    : QDialog(parent), m_ui(new Ui::InsertCarsWindow)
{
    m_ui->setupUi(this);
    QObject::connect(m_ui->buttonBox, &QDialogButtonBox::accepted, this,
                     &InsertCarsWindow::m_insertIntoCars);
}

MainWindow::InsertCarsWindow::~InsertCarsWindow()
{
    delete m_ui;
}

void MainWindow::InsertCarsWindow::m_insertIntoCars()
{
    int id = m_ui->id->value();
    QString num = m_ui->num->text();
    QString color = m_ui->color->text();
    QString mark = m_ui->mark->text();
    int is_foreign = (m_ui->isForeign->isChecked()) ? 1 : 0;

    QString queryText;

    if (id == -1) {
        queryText = QString("INSERT INTO cars (num, color, mark, is_foreign) values (\'%1\', \'%2\', \'%3\', \'%4\');")
                        .arg(num)
                        .arg(color)
                        .arg(mark)
                        .arg(is_foreign);
    } else {
        queryText =
                QString("INSERT INTO cars (num, color, mark, is_foreign) values (%1, \'%2\', \'%3\'), \'%4\', \'%5\');")
                        .arg(id)
                        .arg(num)
                        .arg(color)
                        .arg(mark)
                        .arg(is_foreign);
    }
    QSqlQuery query;
    if (!query.exec(queryText)) {
        qDebug() << "Unable to execute query";
        qDebug() << query.lastError().text();
    } else {
        m_model->select();
    }

    emit event(queryText);

    accept();
}

void MainWindow::InsertCarsWindow::setModel(std::shared_ptr< QSqlTableModel > model)
{
    m_model = model;
}