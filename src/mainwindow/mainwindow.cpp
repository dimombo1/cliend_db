#include "mainwindow.h"

#include <algorithm>

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QFile>

#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      m_ui(new Ui::MainWindow),
      m_insertServicesWindow(new InsertServicesWindow),
      m_insertMastersWindow(new InsertMastersWindow),
      m_insertCarsWindow(new InsertCarsWindow),
      m_updateServicesWindow(new UpdateServicesWindow),
      m_updateMastersWindow(new UpdateMastersWindow),
      m_updateCarsWindow(new UpdateCarsWindow),
      m_deleteFromTableWindow(new DeleteFromTableWindow)
{
    m_ui->setupUi(this);
    m_tableModels.reserve(m_numberOfTables());

    QObject::connect(m_ui->insert, &QAction::triggered, this, &MainWindow::m_insert);
    QObject::connect(m_ui->update, &QAction::triggered, this, &MainWindow::m_update);
    QObject::connect(m_ui->delete_2, &QAction::triggered, this, &MainWindow::m_delete);
    QObject::connect(m_ui->addWork, &QPushButton::pressed, this, &MainWindow::m_addWork);
    QObject::connect(m_ui->updateStatisticsButton, &QPushButton::pressed, this, &MainWindow::m_updateStatistics);
    QObject::connect(m_ui->upload, &QAction::triggered, this, &MainWindow::m_uploadReports);
    QObject::connect(m_ui->logOutButton, &QPushButton::pressed, this, &MainWindow::m_logOut);


    QObject::connect(m_insertServicesWindow, &InsertServicesWindow::event, this, &MainWindow::m_updateJournal);
    QObject::connect(m_insertMastersWindow, &InsertMastersWindow::event, this, &MainWindow::m_updateJournal);
    QObject::connect(m_insertCarsWindow, &InsertCarsWindow::event, this, &MainWindow::m_updateJournal);
    QObject::connect(m_updateServicesWindow, &UpdateServicesWindow::event, this, &MainWindow::m_updateJournal);
    QObject::connect(m_updateMastersWindow, &UpdateMastersWindow::event, this, &MainWindow::m_updateJournal);
    QObject::connect(m_updateCarsWindow, &UpdateCarsWindow::event, this, &MainWindow::m_updateJournal);
    QObject::connect(m_deleteFromTableWindow, &DeleteFromTableWindow::event, this, &MainWindow::m_updateJournal);

    m_tableNames << "works"
                 << "services"
                 << "masters"
                 << "cars";
}

MainWindow::~MainWindow()
{
    m_db.close();

    delete m_ui;
    delete m_insertServicesWindow;
    delete m_insertMastersWindow;
    delete m_insertCarsWindow;
    delete m_updateServicesWindow;
    delete m_updateMastersWindow;
    delete m_updateCarsWindow;
    delete m_deleteFromTableWindow;
}

void MainWindow::userIsAuthorized(const Settings &settings, const QString &login, const QString &password)
{
    show();

    m_settings = settings;
    m_password = password;
    m_ui->loggedAs->setText(login);

    m_connectToDB(login, password);

    const QString event = QString("User %1 connected to db").arg(login);
    m_updateJournal(event);

    if (!m_db.open()) {
        qDebug() << "Ошибка подключения к PSQL";
        qDebug() << QSqlDatabase::drivers();
        qDebug() << m_db.lastError().text();
    } else {
        qDebug() << "Успешное подключение к БД";
    }

    m_tableViews.insert("works", m_ui->works);
    m_tableViews.insert("services", m_ui->services);
    m_tableViews.insert("masters", m_ui->masters);
    m_tableViews.insert("cars", m_ui->cars);

    for (auto &&tableName : m_tableNames) {
        m_fillTable(tableName);
    }

    qDebug() << "here";
    m_updateAssignTab();

    m_insertServicesWindow->setModel(m_tableModels[Models::services]);
    m_insertMastersWindow->setModel(m_tableModels[Models::masters]);
    m_insertCarsWindow->setModel(m_tableModels[Models::cars]);

    m_updateServicesWindow->setModel(m_tableModels[Models::services]);
    m_updateMastersWindow->setModel(m_tableModels[Models::masters]);
    m_updateCarsWindow->setModel(m_tableModels[Models::cars]);
}

void MainWindow::m_connectToDB(const QString &login, const QString &password)
{
    m_db = QSqlDatabase::addDatabase("QPSQL");
    m_db.setHostName("localhost");
    m_db.setDatabaseName(m_settings.databaseName);
    m_db.setUserName(login);
    m_db.setPassword(password);
    m_db.setPort(5432);
    //+журналы (логи - все действия за сессию)
}

void MainWindow::m_fillTable(const QString &tableName)
{
    auto tableModel = std::make_shared<QSqlTableModel>(nullptr, m_db);
    tableModel->setTable(tableName);
    tableModel->select();
    m_tableViews[tableName]->setModel(tableModel.get());
    m_tableModels.push_back(std::move(tableModel));
}

int MainWindow::m_numberOfTables()
{
    return 4; // число таблиц
}

void MainWindow::m_insertIntoTable(const QString &tableName)
{
    if (tableName == "services") {
        m_insertServicesWindow->show();
    } else if (tableName == "masters") {
        m_insertMastersWindow->show();
    }  else if (tableName == "cars") {
        m_insertCarsWindow->show();
    }
    m_updateAssignTab();
}

void MainWindow::m_updateTable(const QString &tableName)
{
    if (tableName == "services") {
        m_updateServicesWindow->show();
    } else if (tableName == "masters") {
        m_updateMastersWindow->show();
    }  else if (tableName == "cars") {
        m_updateCarsWindow->show();
    }
    m_updateAssignTab();
}

void MainWindow::m_deleteTable(const QString &tableName)
{
    std::shared_ptr< QSqlTableModel > model;
    if (tableName == "services") {
        model = m_tableModels[Models::services];
    } else if (tableName == "masters") {
        model =  m_tableModels[Models::masters];
    } else if (tableName == "cars") {
        model =  m_tableModels[Models::cars];
    }
    m_deleteFromTableWindow->setModel(model);
    m_deleteFromTableWindow->setTable(tableName);
    m_deleteFromTableWindow->show();
    m_updateAssignTab();
}

void MainWindow::m_updateAssignTab()
{
    m_updateServiceNames();
    m_updateMastersNames();
    m_updateCarsNums();
}

void MainWindow::m_updateServiceNames()
{
    m_ui->serviceName->clear();
    const QString column = "name";
    const QString table = "services";
    QStringList names = m_selectColumnFromTable(column, table);
    m_ui->serviceName->addItems(names);
}

void MainWindow::m_updateMastersNames()
{
    m_ui->masterName->clear();
    const QString column = "name";
    const QString table = "masters";
    QStringList names = m_selectColumnFromTable(column, table);
    m_ui->masterName->addItems(names);
}

void MainWindow::m_updateCarsNums()
{
    m_ui->carNum->clear();
    const QString column = "num";
    const QString table = "cars";
    QStringList names = m_selectColumnFromTable(column, table);
    m_ui->carNum->addItems(names);
}

void MainWindow::m_updateTotalCostService(const QString &fromDate, const QString &toDate)
{
    QSqlQuery query (m_db);

    const QString queryText = QString("CALL calculate_works_cost(\'%1\', \'%2\'); ").arg(fromDate).arg(toDate);

    if (!query.exec(queryText)) {
        qDebug() << "Query execution error:" << query.lastError().text();
    }

    QSqlQuery second ("SELECT * FROM works_costs_table;");

    second.next();
    double total_foreign_cost = second.value("total_foreign_cost").toDouble();
    double total_our_cost = second.value("total_our_cost").toDouble(); 

    m_ui->costForeign->setText(QString::number(total_foreign_cost));
    m_ui->costOur->setText(QString::number(total_our_cost));

}

void MainWindow::m_updateTopMasters(const QString &targetMonth)
{
    QSqlQuery query (m_db);
    
    const QString queryText = QString("CALL top_masters_by_works(\'%1\');").arg(targetMonth);

    if (!query.exec(queryText)) {
        qDebug() << "Query execution error:" << query.lastError().text();
    }

    QSqlQuery second ("SELECT * FROM temp_works_count;");

    m_topMasters.setQuery(second);

    if (!m_topMasters.lastError().isValid()) {
        m_ui->topMasters->setModel(&m_topMasters);
    } else {
        qDebug() << m_topMasters.lastError();
    }
}

QStringList MainWindow::m_selectColumnFromTable(const QString &column, const QString &table) const
{
    QString queryText = QString("SELECT %1 FROM %2;").arg(column).arg(table);
    QSqlQuery query;
    QStringList returnable;
    if (!query.exec(queryText)) {
        qDebug() << QString("Unable to select %1 from %1").arg(column).arg(table);
    } else {
        while (query.next()) {
            QString name = query.value(column).toString();
            returnable.append(name);
        }
    }
    return returnable;
}

void MainWindow::m_insert()
{
    switch (m_ui->tabWidget->currentIndex()) {
    case Tabs::Services:
        m_insertIntoTable("services");
        break;
    case Tabs::Masters:
        m_insertIntoTable("masters");
        break;
    case Tabs::Cars:
        m_insertIntoTable("cars");
        break;
    default:
        break;
    }
}

void MainWindow::m_update()
{
    switch (m_ui->tabWidget->currentIndex()) {
    case Tabs::Services:
        m_updateTable("services");
        break;
    case Tabs::Masters:
        m_updateTable("masters");
        break;
    case Tabs::Cars:
        m_updateTable("cars");
        break;
    default:
        break;
    }
}

void MainWindow::m_delete()
{
    switch (m_ui->tabWidget->currentIndex()) {
    case Tabs::Services:
        m_deleteTable("services");
        break;
    case Tabs::Masters:
        m_deleteTable("masters");
        break;
    case Tabs::Cars:
        m_deleteTable("cars");
        break;
    default:
        break;
    }
}

void MainWindow::m_addWork()
{
    QString serviceName = m_ui->serviceName->currentText();
    QString masterName = m_ui->masterName->currentText();
    QString carNum = m_ui->carNum->currentText();
    QString date = m_ui->dateWork->date().toString("yyyy-MM-dd");

    const QString getServiceId = QString("SELECT id FROM services WHERE name = \'%1\'").arg(serviceName);
    const QString getMasterId = QString("SELECT id FROM masters WHERE name = \'%1\'").arg(masterName);
    const QString getCarId = QString("SELECT id FROM cars WHERE num = \'%1\'").arg(carNum);

    QSqlQuery serviceQuery;
    QSqlQuery masterQuery;
    QSqlQuery carQuery;

    if (!serviceQuery.exec(getServiceId) || !masterQuery.exec(getMasterId) || !carQuery.exec(getCarId)) {
        qDebug() << "Unable to get data from tables";
        return;
    }

    serviceQuery.next();
    masterQuery.next();
    carQuery.next();
    
    int serviceId = serviceQuery.value("id").toInt();
    int masterId = masterQuery.value("id").toInt();
    int carId = carQuery.value("id").toInt();

    const QString work = QString("INSERT INTO works (date_work, master_id, car_id, service_id) VALUES (\'%1\', %2, %3, %4);")
                                    .arg(date)
                                    .arg(masterId)
                                    .arg(carId)
                                    .arg(serviceId);
    QSqlQuery addWork;

    qDebug() << date << masterId << carId << serviceId;

    m_updateJournal(work);

    if (!addWork.exec(work)) {
        qDebug() << "Unable to add work: " << addWork.lastError().text();
        return;
    }

    m_tableModels[Models::works]->select();
}

void MainWindow::m_updateStatistics()
{
    QString fromDate = m_ui->fromDate->date().toString("yyyy-MM-dd");
    QString toDate = m_ui->toDate->date().toString("yyyy-MM-dd");
    QString targetMonth = m_ui->targetMonth->date().toString("yyyy-MM-dd");

    m_updateTotalCostService(fromDate, toDate);
    m_updateTopMasters(targetMonth);
}

void MainWindow::m_uploadReports()
{
    const QString path = QStringLiteral("reports/report.txt");
    QFile file(path);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qDebug() << "Unable to open file " << path;
        return;
    }

    QTextStream out(&file);

    for (auto && model: m_tableModels) {
        out << m_convertTableToStr(model);
    }

    const QString journalPath = QStringLiteral("reports/journal.txt");

    QFile journalFile (journalPath);

    if (!journalFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qDebug() << "Unable to open file " << path;
        return;
    }

    QTextStream journalOut (&journalFile);

    const QString journalText = m_ui->journal->toPlainText();

    journalOut << journalText;

    file.close();

    const QString event = QStringLiteral("report was uploaded");
    m_updateJournal(event);
}

QString MainWindow::m_convertTableToStr(const std::shared_ptr< QSqlTableModel > model) const
{
    int rowCount = model->rowCount();
    int columnCount = model->columnCount();
    const QString delimiter = QStringLiteral(" ");
    int width = 20;

    QString returnable;

    returnable.append(QString(model->tableName().append("\n")));

    // Записываем заголовки столбцов
    for (int col = 0; col < columnCount; ++col) {
        QString header = model->headerData(col, Qt::Horizontal).toString();
        if (header.size() < width) {
            header.append(QString(width - header.size(), QLatin1Char(' ')));
        }
        returnable.append(header);
    }
    returnable.append("\n");

    // Записываем данные из модели
    for (int row = 0; row < rowCount; ++row) {
        for (int col = 0; col < columnCount; ++col) {
            QModelIndex index = model->index(row, col);
            QString data = model->data(index).toString();
            if (data.size() < width) {
                data.append(QString(width - data.size(), QLatin1Char(' ')));
            }
            returnable.append(data);
        }
        returnable.append("\n");
    }

    returnable.append("\n");

    return returnable;
}

void MainWindow::m_logOut()
{
    m_db.close();
    QSqlDatabase::removeDatabase(m_db.connectionName());
    setVisible(false);
    emit sessionEnded();
}

void MainWindow::m_updateJournal(const QString &event)
{
    m_ui->journal->append(event);
}
