#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <memory>

#include <QMainWindow>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlQueryModel>
#include <QTableView>
#include <QVector>
#include <QMap>
#include <QStringList>
#include <QDialog>
#include "settings/settings.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void userIsAuthorized(const Settings &settings, const QString &login, const QString &password);

signals:
    void sessionEnded();

private:
    Ui::MainWindow *m_ui;
    QSqlDatabase m_db;
    Settings m_settings;
    QString m_password;
    QStringList m_tableNames;

    using TableModels = QVector<std::shared_ptr<QSqlTableModel>>;
    TableModels m_tableModels;
    using TableViews = QMap<QString, QTableView *>;

    enum Models { works, services, masters, cars };

    enum Tabs { Assign, Works, Services, Masters, Cars };

    TableViews m_tableViews;

    class InsertServicesWindow;
    InsertServicesWindow *m_insertServicesWindow;

    class InsertMastersWindow;
    InsertMastersWindow *m_insertMastersWindow;

    class InsertCarsWindow;
    InsertCarsWindow *m_insertCarsWindow;


    class UpdateServicesWindow;
    UpdateServicesWindow *m_updateServicesWindow;

    class UpdateMastersWindow;
    UpdateMastersWindow *m_updateMastersWindow;

    class UpdateCarsWindow;
    UpdateCarsWindow *m_updateCarsWindow;

    class DeleteFromTableWindow;
    DeleteFromTableWindow *m_deleteFromTableWindow;

    QSqlQueryModel m_topMasters;

    void m_connectToDB(const QString &login, const QString &password);
    void m_fillTable(const QString &tableName);
    void m_insertIntoTable(const QString &tableName);
    void m_updateTable(const QString &tableName);
    void m_deleteTable(const QString &tableName);
    void m_updateAssignTab();

    void m_updateServiceNames();
    void m_updateMastersNames();
    void m_updateCarsNums();

    void m_updateTotalCostService(const QString &fromDate, const QString &toDate);
    void m_updateTopMasters(const QString &targetMonth);

    QStringList m_selectColumnFromTable(const QString &columnName, const QString &tableName) const;

    static int m_numberOfTables();

    QString m_convertTableToStr(const std::shared_ptr< QSqlTableModel > model) const;

private slots:
    void m_insert();
    void m_update();
    void m_delete();
    void m_addWork();
    void m_updateStatistics();
    void m_uploadReports();
    void m_logOut();
    void m_updateJournal(const QString &event);
};

QT_BEGIN_NAMESPACE
namespace Ui {
class InsertServicesWindow;
}
QT_END_NAMESPACE

class MainWindow::InsertServicesWindow : public QDialog
{
    Q_OBJECT
public:
    InsertServicesWindow(QWidget *parent = nullptr);
    ~InsertServicesWindow();
    void setModel(std::shared_ptr< QSqlTableModel > model);

signals:
    void event(const QString &event);

private:
    Ui::InsertServicesWindow *m_ui;
    std::shared_ptr< QSqlTableModel > m_model;

private slots:
    void m_insertIntoServices();
};

QT_BEGIN_NAMESPACE
namespace Ui {
class InsertMastersWindow;
}
QT_END_NAMESPACE

class MainWindow::InsertMastersWindow : public QDialog
{
    Q_OBJECT
public:
    InsertMastersWindow(QWidget *parent = nullptr);
    ~InsertMastersWindow();
    void setModel(std::shared_ptr< QSqlTableModel > model);

signals:
    void event(const QString &event);

private:
    Ui::InsertMastersWindow *m_ui;
    std::shared_ptr< QSqlTableModel > m_model;

private slots:
    void m_insertIntoMasters();
};

QT_BEGIN_NAMESPACE
namespace Ui {
class InsertCarsWindow;
}
QT_END_NAMESPACE

class MainWindow::InsertCarsWindow : public QDialog
{
    Q_OBJECT
public:
    InsertCarsWindow(QWidget *parent = nullptr);
    ~InsertCarsWindow();
    void setModel(std::shared_ptr< QSqlTableModel > model);

    signals:
        void event(const QString &event);
    
private:
    Ui::InsertCarsWindow *m_ui;
    std::shared_ptr< QSqlTableModel > m_model;

private slots:
    void m_insertIntoCars();
};

QT_BEGIN_NAMESPACE
namespace Ui {
class UpdateServicesWindow;
}
QT_END_NAMESPACE

class MainWindow::UpdateServicesWindow : public QDialog
{
    Q_OBJECT
public:
    UpdateServicesWindow(QWidget *parent = nullptr);
    ~UpdateServicesWindow();
    void setModel(std::shared_ptr< QSqlTableModel > model);

signals:
    void event(const QString &event);

private:
    Ui::UpdateServicesWindow *m_ui;
    std::shared_ptr< QSqlTableModel > m_model;

private slots:
    void m_updateServices();
};

QT_BEGIN_NAMESPACE
namespace Ui {
class UpdateMastersWindow;
}
QT_END_NAMESPACE

class MainWindow::UpdateMastersWindow : public QDialog
{
    Q_OBJECT
public:
    UpdateMastersWindow(QWidget *parent = nullptr);
    ~UpdateMastersWindow();
    void setModel(std::shared_ptr< QSqlTableModel > model);

signals:
    void event(const QString &event);

private:
    Ui::UpdateMastersWindow *m_ui;
    std::shared_ptr< QSqlTableModel > m_model;

private slots:
    void m_updateMasters();
};

QT_BEGIN_NAMESPACE
namespace Ui {
class UpdateCarsWindow;
}
QT_END_NAMESPACE

class MainWindow::UpdateCarsWindow : public QDialog
{
    Q_OBJECT
public:
    UpdateCarsWindow(QWidget *parent = nullptr);
    ~UpdateCarsWindow();
    void setModel(std::shared_ptr< QSqlTableModel > model);

signals:
    void event(const QString &event);

private:
    Ui::UpdateCarsWindow *m_ui;
    std::shared_ptr< QSqlTableModel > m_model;

private slots:
    void m_updateCars();
};

QT_BEGIN_NAMESPACE
namespace Ui {
class DeleteFromTableWindow;
}
QT_END_NAMESPACE

class MainWindow::DeleteFromTableWindow : public QDialog
{
    Q_OBJECT
public:
    DeleteFromTableWindow(QWidget *parent = nullptr);
    ~DeleteFromTableWindow();
    void setModel(std::shared_ptr< QSqlTableModel > model);
    void setTable(const QString &tableName);

signals:
    void event(const QString &event);
    
private:
    Ui::DeleteFromTableWindow *m_ui;
    std::shared_ptr< QSqlTableModel > m_model;
    QString m_tableName;

private slots:
    void m_deleteFromTable();
};

#endif