if (NOT SRC_INCLUDED)

    set(SRC_INCLUDED TRUE)

    include_directories(${CMAKE_CURRENT_LIST_DIR})

    include(${CMAKE_CURRENT_LIST_DIR}/signinwindow/signinwindow.cmake)
    include(${CMAKE_CURRENT_LIST_DIR}/settings/settings.cmake)
    include(${CMAKE_CURRENT_LIST_DIR}/settingsparser/settingsparser.cmake)
    include(${CMAKE_CURRENT_LIST_DIR}/mainwindow/mainwindow.cmake)


    set(SRC
        ${SIGN_IN_WINDOW}
        ${SETTINGS}
        ${SETTINGS_PARSER}
        ${MAIN_WINDOW}
    )

endif()