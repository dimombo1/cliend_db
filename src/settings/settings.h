#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>
#include <QMap>

struct User
{
    QString login;
    QString password;
};


struct Settings
{
    QString type;
    QString connectionName;
    QString databaseName;
    QMap< QString, User > users;

    static int getNumberOfSettings();
};

#endif