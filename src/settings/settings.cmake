if (NOT SETTINGS_INCLUDED)
    set(SETTINGS_INCLUDED TRUE)
    include_directories(${CMAKE_CURRENT_LIST_DIR})

    set(SETTINGS
        ${CMAKE_CURRENT_LIST_DIR}/settings.h
        ${CMAKE_CURRENT_LIST_DIR}/settings.cpp
        )
endif()